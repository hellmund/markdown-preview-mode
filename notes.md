

* aus .emacs:

``` elisp
(autoload 'markdown-mode "markdown-mode.el"
  "Major mode for editing Markdown files" t)
(setq auto-mode-alist
      (cons '("\\.text" . markdown-mode) auto-mode-alist))
(setq markdown-enable-math t)
;;(setq markdown-split-window-direction 'right)
(setq browse-url-browser-function #'browse-url-chromium)
(setq markdown-command
      (concat
       "/usr/bin/pandoc --to=html"
       " --from markdown+tex_math_dollars+yaml_metadata_block+autolink_bare_uris"
       " --mathjax --highlight-style=pygments"))
    ;;   " --standalone --toc"
;;M-x package-refresh-contents
;;M-x package-install Ret web-server
;;(require 'markdown-preview-mode)

```


* dann

``` elisp
M-x package-install markdown-preview-mode
```

* dann `preview.html` in `.emacs.d/elpa/m-p-m`   ersetzen,

* dann in einem md-buffer:   `M-x markdown-preview-mode`


(Zum unterschied: `M-x markdown-preview` macht auch ein chrome-buffer
auf, aber ohne live-update)

